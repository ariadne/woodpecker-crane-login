module gitea.treehouse.systems/ariadne/woodpecker-crane-login

go 1.17

require (
	github.com/docker/cli v20.10.17+incompatible
	github.com/google/go-containerregistry v0.11.0
	github.com/joho/godotenv v1.4.0
	github.com/urfave/cli v1.22.9
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/docker/docker v20.10.17+incompatible // indirect
	github.com/docker/docker-credential-helpers v0.6.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
